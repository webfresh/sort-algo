// Imports
import { RandomBars } from "./RandomBars";

// Main Routine
console.info("Loading DOM");
document.addEventListener("DOMContentLoaded", () => {
  console.info("DOM Loaded!");
  
  // Generate Bars
  const bars = new RandomBars();
  const generateButton = document.getElementById("generateBars");
  generateButton.addEventListener("click", () => {
    let num = parseInt(document.getElementById("numBars").value);
    bars.generateBars(num);

    // Get Numbers from Bars
    const axis = document.getElementById("axis");
    let generatedBars = [...axis.children];
    generatedBars = generatedBars.map(e => parseInt(e.style.height.replace('%','')));
    console.clear();
    console.table(generatedBars);
  });
});