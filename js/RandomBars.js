export class RandomBars {
	generateBars(num) {
    let rand = 0;
    while (num > 0) {
      rand = Math.round(Math.random() * 100);
      this.addBar(rand);
      num--;
    }
  }
  
	addBar(height) {
    let axis = document.getElementById("axis")
    let newBar = document.createElement("div");
    newBar.style.height = `${height}%`;
    newBar.classList.add("bar");
    axis.appendChild(newBar);
	}
}